### 自我介绍

1. 面试官您好，我是刘杰，来自安徽科技学院计算机科学与技术专业，
2. 在校期间学习了c++、Java基础、Java框架、算法分析、web前端设计等课程，
3. 也拿到过省级ACM竞赛三等奖和校级ACM竞赛一等奖，在前端这方面现在我已经能够快速还原设计稿，
4. 并且我也从0到1做过电商管理系统项目，使用vue全家桶结合elementPlus组件库进行页面搭建和业务逻辑的书写，
5. 用预编译语言SCSS编写样式， 利用axios实现前后端数据交互，
6. 也使用了ts对接口返回的数据以及向服务器提交的数据进行类型定义，
7. 在这个过程中也会通过idea向git上推送。之前也买过云服务器玩，
8. 也将做的项目打包部署在服务器上，平常的话也会做一做算法题，至今在csdn上也发表了近80篇算法题解。
## 介绍项目
1. 项目背景：
这个项目的话主要是对用户权限的管理，和对各种品牌以及对每种品牌下的商品进行分类等操作，<br>
也实现了对商品信息的增删改查，也可以对商品设置上架与下架两种状态。<br>
2. 技术栈：使用Vue进行组件化开发，提高了代码的可维护性，<br>
利用elementPlus组件库，进行快速的页面搭建<br>
使用了ECharts绘制了可视化大屏，显示销售数据
3. 项目亮点：业务比较常规，确实没有什么特别难点，但我希望在以后工作中可以遇到，<br>这样的话更能磨练我，能够自我提升。
   - 用户权限管理：在项目中我们实现了严格的用户权限管理，不同角色的用户拥有不同的权限，<br>
如管理员可以管理所有商品，普通用户只能管理自己发布的商品。这种权限管理方式有效保护了数据的安全性。
   - 路由鉴权：通过后端返回该用户拥有的权限，分配对应的路由访问权
4. 总结：现在正处于学习阶段，做这个项目算是有两个目的吧，<br>
   第一就是能熟悉技术栈在项目中的实际运用，<br>
   第二就是通过做项目的过程，能简单了解实际开发的大概流程，熟悉开发中的业务逻辑。<br>
   提升发现问题的能力，和解决问题的能力。
## 职业规划
对现在的我来说，还是要不断地学习掌握更深的前端技术，通过参与一些项目，提升解决问题的能力，<br>
积累更多的经验，也要像前辈学习如何管理项目进度、团队协作，还有如何保证代码质量等方面。<br>
希望能在未来的几年里晋升为高级前端工程师，成为团队的核心成员之一。<br>
当然我也希望能够领导一个小团队，为公司创造更多的价值。<br>
还有就是像我们这些技术岗，它的技术迭代还是蛮快的，所以平常的话还是要坚持学习。

## 场景题
### 如何无感刷新token
1.  - 后端返回过期时间，前端每次请求就判断token的过期时间，快到期了就去调用刷新token接口。
    - 缺点：本地时间会被篡改，服务器可能会拦截失败
2.  - 写个定时器，然后定时刷新token接口。
    - 缺点：浪费资源，消耗性能。
3. - 在请求响应拦截器中拦截，判断token返回过期后，调用刷新token接口。
### token和cookie的区别
1. Token是一种在客户端和服务器之间传递身份信息的机制，可以存储在Cookie中或通过其他方式进行传递。
2. Cookie是一种在客户端存储数据的机制，用于在浏览器和服务器之间传递数据。
### 什么是XSS和CSRF漏洞
1. xss(跨站脚本攻击)：通过表单、URL参数、cookie和HTTP头等地方注入恶意脚本。
2. csrf(跨站请求伪造)：通过伪装来自受信任用户的请求，来攻击受信任的网站。
### 如何防止xss、csrf漏洞
1. xss：
   - 输入验证：过滤或删除输入中的标签和特殊字符`<`和`>`等，这些字符可以用于注入恶意脚本。
   - 输出转义：对特殊字符进行转义
   - 设置CSP(Content Security Policy):<br>
   ```
   <meta http-equiv="Content-Security-Policy" 
         content="style-src 'self'; 
                  script-src 'self'; 
                  form-action 'self'">
    ```
2. csrf：
    - 使用post请求：一定程度上降低csrf攻击风险
    - 使用验证码：提交之前输入验证码，可以确保是用户行为
    - 验证referer：http头上有referer字段，能记录当前请求的来源地址，<br>后端判断referer的值和当前网址域名是否相同
    - anti csrf token：服务端生成token返回时，存在session中，<br>提交时将token带给服务端验证，之后销毁token

## 一、HTML
### HTML5语义化

1. 可以使页面结构更加清晰，方便用户阅读和理解
2. 有利于SEO，提高网页的排名和可见性
3. 方便其他设备解析：阅读器等

- header：定义文档的页眉（头部）；
- nav：定义导航链接的部分；
- main：定义文档的主要内容区域
- footer：定义文档或节的页脚（底部）；
- article：定义文章内容；
- section：定义文档中的节（section、区段）；
- aside：定义其所处内容之外的内容（侧边）；

## 二、CSS
### 说一说BFC
1. 什么是BFC?
    - BFC就是一个块级元素，块级元素会在垂直方向一个接一个的排列
    - BFC就是页面中的一个隔离的独立容器，容器里的标签不会影响到外部标签
    - 垂直方向的距离由margin决定， 属于同一个BFC的两个相邻的标签外边距会发生重叠
2. BFC解决了什么问题？
   - 使用Float脱离文档流，高度塌陷(给父元素添加`display:inline-block`)
   - margin外边距重叠问题 
### css水平垂直居中

1. 有高和宽：`margin-top:负一半;margin-left:负一半`
2. 定位：四个方向都为0，`margin: auto`
3. 偏移：`left:50%;top:50%`,`transform：translate(-50%,-50%)`
4. flex布局：`align-items:center;justify-content:center`
### flex布局

## 三、JavaScript
### ES6的新特性有哪些?
1. 新增块级作用域(let,const)
    - 不存在变量提升
    - 存在暂时性死区的问题
    - 块级作用域的内容
    - 不能在同一个作用域内重复声明
2. 新增了定义类的语法糖(class)
3. 新增了一种基本数据类型(symbol)
4. 新增了解构赋值
    - 从数组或者对象中取值，然后给变量赋值
5. 新增了函数刻数的默队值
6. 给数组新增了API
7. 对象和数组新增了扩展运算符
8. Promise
    - 解决回调地狱的问题。
    - 自身有all,reject,resolve,race方法原型上有then,catch
    - 把异步操作队列化
    - 三种状态: pending初始状态,fulfilled操作成功,rejected操作失败
    - 状态: pending -> fulfilled;pending -> rejected一旦发生，状态就会凝固，不会再变
    - async await
        - 同步代码做异步的操作，两者必须搭配使用
        - async表明函数内有异步操作，调用函数会返回promise
        - await是组成async的表达式，结果是取决于它等待的内容，如果是promise那就是promise的结果，如果是普通函数就进行链式调用
        - await后的promise如果是reject状态，那么整个async函数都会中断，后面的代码不执行
9. 新增了模块化(import,export)
10. 新增了set和map数据结构
    - set就是不重复
    - map的key的类型不受限制
11. 新增了generator
12. 新增了箭头函数
    - 不能作为构造函数使用，不能用new
    - 箭头函数就没有原型
    - 箭头函数没有arguments
    - 箭头函数不能用call,apply,bind去改变this的执行
    - this指向外层第一个函数的this
### var、let、const区别

1. var存在变量提升，而let和const没有
2. var可以重复声明变量，由后面的变量覆盖前面的变量，let和const不可以重复声明
3. let和const有块级作用域，var没有
4. var声明的全局变量会将这个变量添加为全局对象的属性，但是let和const不会
5. 在声明变量时，var和let可以不设置初始值，但const必须要设置
6. 还会有暂时性死区问题，在let和const声明变量之前，这个变量是不能用的，var的话有变量提升，不会有暂时性死区问题。
### 数组去重方式

1. ``let newArr = [...new Set(arr)]``
2. ``arr.filter((value, index) => arr.indexOf(value) === index)``
3. 用for循环或forEach，结合indexOf
4. 用map对象，利用for循环判断map是否含有这个key
5. reduce

```
   arr.reduce((ans, value) => {
    if (!array.includes(value)) array.push(value)
    return array
    }, array)
```
### js事件循环

- js是一个单线程的脚本语言<br>
- 主线程 执行栈 任务队列 宏任务 微任务<br>
- 主线程先执行同步任务，然后才去执行任务队列里的任务，先执行微任务,再执行宏任务<br>
- 全部执行完之后等待主线程的调用，调用完之后再去任务队列中查看是否有异步任务，这样一个循环往复的过程就是事件循环!
1. 宏任务主要包含：script(整体代码)、setTimeout、setInterval、UI交互事件
2. 微任务主要包含：Promise、MutationObserver(监听DOM变化)
### script标签上的defer和async有什么区别

defer是“页面渲染完再执行”，async是“下载完就执行”
### 闭包

就是函数嵌套函数，并且里面的函数可以访问外层函数作用域中的变量
### 防抖节流

1. 防抖：用户多次触发同一个事件，只执行最后一次
    - 应用于输入框的自动保存事件
   ```
   function debouce(func, time){
        let timer
        return ()=>{
            if(timer)clearTimeout(timer)
            timer = setTimeout(()=>{
                func()
            },time)
        }
   }
   ```
2. 节流：把频繁触发的事件减少，每隔一段时间执行一次
### 原型链

1. 原型：js的函数都有prototype属性，这个属性是一个指向它的原型对象的指针
2. 根据这个构造函数`new`出来的对象会有`__proto__`属性，
3. 找对象的属性时，如果自己身上没有，就会通过`__proto__`找到构造函数的prototype，如果没有<br>
   会一直根据这条链找到object.prototype
### 深拷贝与浅拷贝

1. 浅拷贝：是复制对象的一部分或全部属性，但复制出来的对象与原对象共享一块内存，如果改变其中一个，另一个也会跟着改变。
2. 深拷贝：是复制对象的全部属性以及嵌套对象，复制出来的对象与原对象不共享内存，也就是说改变其中一个，另一个不会跟着改变
### 如何实现一个深拷贝?

1. 扩展运算符
    - 缺点：只能对第一层进行深拷贝，当有多层的时候还是浅拷贝
2. JSON.parse(JSON.stringify())
    - 缺点：不能拷贝内部的函数
3. 递归函数实现
### Ajax实现步骤

```
var xhr = new XMLHttpRequest(); //创建XMLHttpRequest对象
xhr.open('GET', '/api/data', true); //配置请求参数
xhr.onreadystatechange = function() {  
  // 当 readyState 等于 4 且状态为 200 时
  if (xhr.readyState === 4 && xhr.status === 200) {  
    let data = JSON.parse(xhr.responseText);  
    console.log(data);  
  }  
};  
xhr.send();//发送请求
```

## 四、promise
### promise的内部原理是什么?它的优缺点是什么?

- Promise对象，封装了一个异步操作并且还可以获取成功或失败的结果<br>
- Promise主要就是解决回调地狱的问题，之前如果异步任务比较多，同时他们之间有相互依赖的关系，<br>
  就只能使用回调函数处理，这样就容易形成回调地狱，代码的可读性差，可维护性也很差<br>
- 有三种状态: pending初始状态、fulfilled成功状态、rejected失败状态<br>
- 状态改变只会有两种情况：<br>
    - pending -> fulfilled; pending -> rejected 一旦发生，状态就不会再变<br>
- 首先就是我们无法取消promise，一旦创建它就会立即执行，不能中途取消<br>
- 如果不设置回调，promise内部抛出的错误就无法反馈到外面<br>
- 若当前处于pending状态时，无法得知目前在哪个阶段。<br>
- 原理:<br>
    - 构造一个Promise实例，实例需要传递函数的参数，这个函数有两个形参，分别都是函数类型，一个是resolve一个是reject<br>
    - promise上还有then方法，这个方法就是来指定状态改变时的确定操作，resolve是执行第一个函数，reject是执行第二个函数<br>
### 常用的promise方法
1. resolve方法
   - Promise.resolve 返回一个成功`(fulfilled)`状态的`promise`
2. all方法
   - `Promise.all(PromiseArr)`传入一个promise对象数组，只有这个数组中
   - 所有的promise状态变为resolve或reject时，才会调用then方法。
   - promise.all的promise是同时开始、并行执行。
### promise和async await的区别是什么?

1. 都是处理异步请求的方式
2. promise是ES6，async await是ES7的语法
3. async await是基于promise实现的，他和promise都是非阻塞性的
4. 优缺点:
    1. promise是返回对象我们要用then，catch方法去处理和捕获异常，并且书写方式是链式，容易造成代码重叠，
       不好维护，async await是通过tra catch进行捕获异常
    2. async await最大的优点就是能让代码看起来像同步一样，只要遇到await就会立刻返回结果，<br>
       然后再执行后面的操作promise.then()的方式返回，会出现请求还没返回，就执行了后面的操作
### 同步和异步的了解

1. 同步：就是如果有一个请求，就会一直等待这个请求返回的结果，直到收到返回信息才继续执行
2. 异步：不会等待这个请求的结果就会往下执行，当信息返回时，再来处理

## 五、Vue
### Vue2响应式原理

通过数据劫持和依赖收集来实现，

1. 数据劫持利用Object.defineProperty方法将对象的每一个属性转化成set和get方法
    - 修改时 -> 触发get
    - 使用该属性时 -> 触发set
2. 依赖收集则是在使用Object.defineProperty方法设置get和set方法时，<br>
   将数据变成响应式数据，set中会接收新的值，取代以前的值
### Vue3响应式原理

通过proxy监听对象，更好的解决了Vue2中无法追踪直接给对象添加属性的缺点。
### Vue2与Vue3区别

1. 响应式原理不同
2. Vue3支持碎片化（可以有多个根节点）
3. api不同
4. 生命周期不同
### 组件通信

1. 父向子传递数据，子组件用props接收
2. 子向父传递数据：用`$emit`绑定自定义事件，这个事件被执行时将参数传给父组件，<br>
   父组件通过`v-on`绑定事件名接收参数
3. $bus:挂载到vue实例上，A组件通过$bus.$emit发送事件，B组件通过$bus.$on接收
4. ref:在父组件中`this.$refs.child.func`使用子组件属性和方法
5. 依赖注入：父组件中provide{ return {}}，子组件中inject:[]
### axios二次封装

1. 在`axios.create()`方法里设置baseURL、timeout、headers等
2. 在请求拦截器`interceptors.request.use`可以为请求中加上`token`
### Computed 和 Watch 的区别

1. computed:
    - 它支持缓存，只有依赖的数据发生了变化，才会重新计算
    - 不支持异步，当Computed中有异步操作时，无法监听数据的变化
    - computed的属性值是函数的返回值
2. watch:
    - 它不支持缓存，数据变化时，它就会触发相应的操作
    - 支持异步监听
    - watch监听的数据必须是data或props传过来的数据
3. 在响应拦截器`interceptors.response.use`中可以对请求异常状态进行处理
### keep-alive的理解

可以在动态组件被切换时缓存他的实例，(触发deactivated钩子)不用重复渲染DOM,也减少加载时间及性能消耗，<br>
提高了用户体验性

## 六、浏览器问题
### 跨域问题

1. cors：设置Access-Control-Allow-Origin: "*"
2. jsonp
3. vite配置：

```
export default defineConfig({
  // 代理配置
  server: {
  	https: true,
    proxy: {
      '/api': { // 配置需要代理的路径 --> 这里的意思是代理http://localhost:80/api/后的所有路由
        target: 'https://172.20.9.153:8085', // 目标地址 --> 服务器地址
        changeOrigin: true, // 允许跨域
        ws: true,  // 允许websocket代理
		// 重写路径 --> 作用与vue配置pathRewrite作用相同
        rewrite: (path) => path.replace(/^\/api/, "")
      }
     },
  },
});
```
### cookies、sessionStorage、localStorage

1. 存储大小不同：cookie大约有4kb，后两者有5MB
2. 生命周期不同：cookie可由后端设置期限，sessionStorage只在当前会话窗口有效，local一直有效
3. 与服务端通信不同：cookie是在http头中自动携带，后两者不可以
### http和https区别

1. HTTP协议是超文本传输协议，信息是明文传输的，HTTPS则是具有安全性的SSL加密传输协议；
2. 使用不同的连接方式，端口也不同，HTTP协议端口是80，HTTPS协议端口是443
### 输入url到页面显示的过程

浏览器会首先检查本地缓存中是否有该URL的缓存，如果有则直接加载缓存内容，<br>
否则DNS服务器会将域名解析为对应的IP地址，然后浏览器向该IP地址发出HTTP请求，<br>
服务器接收到请求后会进行相应的处理，然后返回数据，<br>
浏览器根据接收到的数据构建DOM树和CSS规则树，然后开始渲染页面。
### TCP的三次握手与四次挥手

1. 三次握手
    - 第一次握手：客户端发送 SYN 包，表示自己的初始序号，要求建立连接；然后客户端便进入 SYN-sent 状态。
    - 第二次握手：服务器发送 SYN+ACK 包，表示确认连接请求；进入 SYN-received 状态
    - 第三次握手：客户端发送 ACK 包，表示连接建立成功。双方都进入 established 状态
2. 四次挥手
### rem做移动端适配

1. 设置视口``<meta name="viewport" content="width=device-width, ``<br>``
   user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">``
2. 用js动态修改html的font-size 如: font-size = 屏幕宽度 / 10
3. 换算rem 如： 182 / 75